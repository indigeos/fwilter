# FWilter
FWilter is a portmanteau for 'firewall filter'. Its purpose is to block connectivity from hackers by profiling basic transactions at a protocol level in an attempt to classify a hacker's tool and block it.

FWilter consumes data from the network edge or on east/west traffic and is trained by 3rd party cybersecurity environments. It uses existing cybersecurity systems that have previously classified attacks to attempt to more quickly  classify subsequent attack data at early phases of a new attempt. 

FWilter focuses on advanced persistent threats (APT) which use coordinated and distributed networks for denial of service (DoS) and Brute Force attacks. FWilter exploits the fact that most distributed attacks also distribute the software involved and that this software will behave in similar ways even between multiple, distributed hosts. Third party software, like fail2ban and others, provide feedback loops for data not stopped and FWilter will adjust FWilter's algorithms via machine learning analysis on attack patterns. It aims to identify and block botnet traffic based repetitive patterns.

![Stop before auth](image.png)

# Architecture
FWilter is composed of:
* a controller engine
  * This engine spawns and manages processes like packet capture and collectors
  * Reviews buffer size and will suspend packet capture under stress
  * Will launch additional agents if needed
* packet capture
  * an agent that watches for and collects specified data flow and places them into a buffer for collectors
  * logs unmatched data into feedback feature log
  * pattern matches specific metadata related to targetted services
* collector threads
  * collector threads 
* feedback feature finder
  * gathers information and automates the feature selection process for new attack vectors
  * employs an AI of AI model to select the most performant options based on prior deployments
    * automates feature selection
    * automates performance outcomes from good and bad deployments
* itsjustme
  * a lightweight feedback tool for providing 'ham' from benign actors
  * allows opt-in or scripted feedback for authorized users to feed their connections to the fwilter


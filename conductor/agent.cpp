// David Loper
// University of North Dakota
// Ronald Marsh, PhD - Instructor

#include <iostream>  // needed for cerr and others
#include <ostream>   // needed for endl and others
#include <semaphore.h> // needed to use semaphore
#include <sys/mman.h>  // needed for shm_open (shared memory)
#include <fcntl.h>     // needed for O_CREAT
#include <fstream>     // needed for ifstream
#include <chrono>      // needed for microdelay
#include <thread>      // needed for microdelay
#include <string>      // needed to convery position to string
#include <unistd.h>     // For read, write, close
#include <cstdlib>      // For exit, EXIT_FAILURE
#include <cstring>      // For memset
#include <sys/socket.h> // For socket operations
#include <random>       // For random number generation

void writeToSocket(int socketFd, const std::string& data) {
    ssize_t bytesWritten = write(socketFd, data.c_str(), data.size());
    if (bytesWritten == -1) {
        perror("Error writing to socket");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 6) {
        std::cerr << "Usage: ./grandchild [datafile] [socketFdStr] [id] [semaphoreName] [sharedMemoryName]" << std::endl;
        std::cerr << "NOTE: This process should NOT be individually called from command line." << std::endl;
        return EXIT_FAILURE;
    }

    // Pull in arguments
    std::string proc_id = argv[3];
    std::string datafile = argv[1];
    int socketFd = std::stoi(argv[2]);  // Convert socket file descriptor from string to int
    const char* semaphoreName = argv[4];
    const char* sharedMemoryName = argv[5];
    int proc_id_int = std::stoi(proc_id);  // Convert proc_id from string to int
    proc_id_int += 1;                      // Add one to the integer value of proc_id
    std::string proc_id_plus_one = std::to_string(proc_id_int);  // Convert it back to string

    // Seed the RNG
    srand(time(0));

    // Open semaphore
    sem_t* semaphore = sem_open(semaphoreName, 0);
    if (semaphore == SEM_FAILED) {
        perror("sem_open");
        return EXIT_FAILURE;
    }

    // Open shared memory
    int shm_fd = shm_open(sharedMemoryName, O_RDWR, 0666);
    int* shared_memory = (int*)mmap(0, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);

    // open file for streaming
    std::ifstream file(datafile);
    if (!file.is_open()) {
        std::cerr << "Unable to open file: " << datafile << std::endl;
        return EXIT_FAILURE;
    }

    while (true) {
        sem_wait(semaphore);

        int position = *shared_memory;
	if (position < 0) {
        //if (position == -3) {
	    *shared_memory = *shared_memory * -1;
	    std::string sharedMemoryValueStr = std::to_string(*shared_memory);
            sharedMemoryValueStr += "_____";  // Append five underscores to the string
            if (write(socketFd, sharedMemoryValueStr.c_str(), sharedMemoryValueStr.size()) < 0) {
                std::cerr << "Error sending data" << std::endl;
            }
            *shared_memory = -2;
            break;
        }
        if (position == -2) {
            break;
        }
        std::string output_string;
        int random_num = rand() % 10 + 1;
        int characters_read = 0; // Counter for the number of characters read
        file.seekg(position);
        if (!file.eof() && file.good()) {
            for (int i = 1; i <= random_num; ++i) {
                char ch;
                if (file.get(ch)) {
                    output_string += ch;
                    characters_read++; // Increment the counter
                } else {
                    break;
                }
            }
            output_string = proc_id_plus_one + output_string;
            std::string result = "_p" + output_string;
            std::streampos current_position = file.tellg(); // Get the current position in the file.
            if (current_position != static_cast<std::streampos>(-1)) {
                if (result.empty()) {
                    break; // Break out of the loop if the 'result' string is empty
                }
                writeToSocket(socketFd, result);
                *shared_memory = current_position;
            } else {
                if (result.empty()) {
                    break; // Break out of the loop if the 'result' string is empty
                }
                writeToSocket(socketFd, result);
                *shared_memory += characters_read;
		*shared_memory = *shared_memory * -1;
            }
        } else {
            break;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(3));
    }
    return 0;
}

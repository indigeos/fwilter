// David Loper
// University of North Dakota
// Ronald Marsh, PhD - Instructor

// Place necessary headers
#include <iostream>    // needed for cerr and others
#include <ostream>     // needed for endl and others
#include <string>      // needed for std::string and others
#include <semaphore.h> // needed to createSemaphore
#include <fcntl.h>     // needed for O_CREAT
#include <sys/mman.h>  // needed for shm_open (shared memory)
#include <unistd.h>    // needed for ftruncate and others
#include <sys/socket.h>// needed for AF_UNIX and SOCK_STREAM
#include <cstring>     // needed for memset
#include <fstream>     // needed to output to file

////////////////////////////////////////////////////////
// Global variables                                   //
////////////////////////////////////////////////////////
const int NUM_PROCESSES = 9;                          // Number of processes
const char* SEMAPHORE_NAME = "/gp_semaphore";         // Semaphore (named)
const char* SHARED_MEMORY_NAME = "/gp_sharedmemory";  // Shared memory (named)
int sockets[NUM_PROCESSES][2];                        // Number of sockets
const char* OUTFILE = "hw7.out";                      // Output file
////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
// Spawn a grandchild, heavyweight process                     //
/////////////////////////////////////////////////////////////////
int createChildProcess(int id, const std::string& datafile) {
    int sockets[2];  // Array to store the file descriptors of the socket pair

    // Setup pipe
    if (socketpair(AF_UNIX, SOCK_STREAM, 0, sockets) < 0) {
        perror("socketpair");
        exit(EXIT_FAILURE);  // Error in creating socket pair, exit the program
    }

    pid_t pid = fork();  // Fork grandpa
    if (pid == 0) {      // Detect if we are a child
        close(sockets[0]);  // Close the parent's end of the socket in the child process

        // Convert socket file descriptor to string to pass as an argument
        char socketFdStr[10];
        char idStr[10];
        sprintf(socketFdStr, "%d", sockets[1]);
        sprintf(idStr, "%d", id);  // Convert the ID to a string
        // Execute the child process with necessary arguments
        execlp("./grandchild", "grandchild", datafile.c_str(), socketFdStr, idStr, SEMAPHORE_NAME, SHARED_MEMORY_NAME, nullptr);
        exit(EXIT_FAILURE);  // If we cannot exec, kill fake grandpa
    } else if (pid > 0) {
        close(sockets[1]);  // Close the child's end of the socket in the parent process
        return sockets[0];  // Return the parent's end of the socket
    } else {
        perror("fork");
        exit(EXIT_FAILURE);  // Error in fork, exit the program
    }
}

///////////////////////////////////////////////////////////
// Function to create and initialize semaphore           //
///////////////////////////////////////////////////////////
sem_t* createSemaphore(const char* name) {               // Start semaphore function
    sem_unlink("/gp_semaphore");                         // Unlink the semaphore
    sem_t* semaphore = sem_open(name, O_CREAT, 0666, 1); // Create the semaphore using the name passed
    if(semaphore == SEM_FAILED) {                        // Condition: Detect if this failed
        perror("sem_open");                              //   Throw error
        exit(EXIT_FAILURE);                              //   Exit on failure
    }                                                    // Condition complete
    return semaphore;                                    // Return semaphore value
}                                                        // Close function
///////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
// Function to create shared memory                                            //
/////////////////////////////////////////////////////////////////////////////////
int* createSharedMemory(const char* name, size_t size) {
    bool newlyCreated = false;
    int shm_fd = shm_open(name, O_CREAT | O_EXCL | O_RDWR, 0666); // Try creating new shared memory
    if (shm_fd == -1) {
        if (errno == EEXIST) { // Shared memory already exists
            shm_fd = shm_open(name, O_RDWR, 0666); // Open existing shared memory
            if (shm_fd == -1) {
                perror("shm_open existing");
                exit(EXIT_FAILURE);
            }
        } else { // Other errors
            perror("shm_open new");
            exit(EXIT_FAILURE);
        }
    } else {
        newlyCreated = true; // New shared memory created
    }

    if (ftruncate(shm_fd, size) == -1) {
        perror("ftruncate");
        exit(EXIT_FAILURE);
    }

    void* addr = mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (addr == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    if (newlyCreated) {
        memset(addr, 0, size); // Initialize newly created shared memory with 0
    }

    return static_cast<int*>(addr);
}


//////////////////////////////////
// Main function                //
//////////////////////////////////
int main(int argc, char *argv[]) {
    ////////////////////////////////////////////////////////////////////////////
    // "Grandpa will get the data filename as a command-line argument."       //
    ////////////////////////////////////////////////////////////////////////////
    if (argc != 2) {                                                          //
        std::cerr << "Usage: " << argv[0] << " <datafile_path>" << std::endl; //
        return EXIT_FAILURE;                                                  //
    }                                                                         //
    std::string datafile = argv[1];                                           // Parse command-line arguments for data file

    ////////////////////////////////////////////////////////////////////////////
    // "Grandpa will then create the 9 pipes,"                                //
    ////////////////////////////////////////////////////////////////////////////
    // we will do this dynamically as we create the 9 child processes         //
    int parent_sockets[NUM_PROCESSES];  // Array to store parent sockets

    ////////////////////////////////////////////////////////////////////////////
    // "1 semaphore,"                                                         //
    ////////////////////////////////////////////////////////////////////////////
    sem_t* semaphore = createSemaphore(SEMAPHORE_NAME);                       // Call to create shared semaphore

    ////////////////////////////////////////////////////////////////////////////
    // "and 1 shared memory (initialized to 0)."                              //
    ////////////////////////////////////////////////////////////////////////////
    int* sh_mem = createSharedMemory("/gp_sharedmemory", sizeof(int));        // Call to create shared memory
    *sh_mem = 0; // Set the shared memory value to 0

    ////////////////////////////////////////////////////////////////////////////
    // " Grandpa will then make sure the semaphore is free (unlocked)."       //
    // sem_post(semaphore);                                                      // Unlock semaphore

    ////////////////////////////////////////////////////////////////////////////
    // "Grandpa will then fork & exe the nine Grandchildren "                 //
    // "and pass on to them ... as command-line arguments."                   //
    // " * the data filename,"                                                //
    // " * the pipe identifiers,"                                             //
    // " * semaphore identifier, and "                                        //
    // " * the shared memory identifier .... "                                //
    // "Grandpa simply named his Grandchildren: p1, p2, … p9."                //
    ////////////////////////////////////////////////////////////////////////////

    for(int i = 0; i < NUM_PROCESSES; ++i) {
        parent_sockets[i] = createChildProcess(i, datafile);  // Store parent socket while creating procs
    }

    // Setup the output file
    std::ofstream outFile("hw7.out", std::ios::out); // Open hw7.out for appending


    ///// READ OUT THE DATA ////
    fd_set readfds;
    int max_fd = 0;
    for (int i = 0; i < NUM_PROCESSES; ++i) {
        if (parent_sockets[i] > max_fd)
            max_fd = parent_sockets[i];  // Find the maximum file descriptor number
    }

    while (true) {
        FD_ZERO(&readfds);
        for (int i = 0; i < NUM_PROCESSES; ++i) {
            FD_SET(parent_sockets[i], &readfds);  // Add each socket to the set
        }
        // Wait for data on any socket, timeout is NULL for indefinite wait
        int activity = select(max_fd + 1, &readfds, NULL, NULL, NULL);

        if (activity < 0 && errno != EINTR) {
            perror("select error");
            exit(EXIT_FAILURE);
        }


        for (int i = 0; i < NUM_PROCESSES; ++i) {
            if (FD_ISSET(parent_sockets[i], &readfds)) {
                char buffer[1024];
                int bytes_read = read(parent_sockets[i], buffer, sizeof(buffer) - 1);
                if (bytes_read > 0) {
                    buffer[bytes_read] = '\0';  // Null-terminate the string

                    std::string bufferStr(buffer); // Convert to std::string for easier processing
                    if (bufferStr.size() >= 5 && bufferStr.compare(bufferStr.size() - 5, 5, "_____") == 0) {
                        //std::cout << "Data from process " << i << ": " << buffer << std::endl;
                        bufferStr.erase(bufferStr.size() - 5, 5);
                        //std::cout << "Total Characters: " << bufferStr << std::endl;
                        outFile << bufferStr; // Write the modified string to the file
                        outFile.close(); // Close the file
                        return 0; // Exit the program or break out of the loop as needed
                    } else {
                        if (strlen(buffer) > 3) {
                            //std::cout << "Data from process " << i << ": " << buffer << std::endl;
                            outFile << buffer;
                        }
		    }

                }
            }
        }
        sem_post(semaphore);
    }
    return 0;                                                           // All done
}



